import { Component } from '@angular/core';
import { Subscription } from 'rxjs';
import { PokemonModule } from 'src/app/modules/pokemon-module';
import { PokemonService } from 'src/app/services/pokemon.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.css'],
})
export class TrainerComponent {
  //Subscriptions
  public pokemonSubscription$: Subscription;
  public trainerSubscription$: Subscription;

  //Local state
  public allPokemons: PokemonModule[] = [];
  public pokemons: PokemonModule[] = [];
  pokemonPage: any[] = [];
  pages: number[] = [];
  pageLength: number = 27;

  constructor(
    private readonly pokemonService: PokemonService,
    private readonly trainerService: TrainerService
  ) {
    //subscribes to pokemon
    this.pokemonSubscription$ = pokemonService.pokemons$.subscribe(
      (pokemons) => {
        this.allPokemons = pokemons;
      }
    );

    //Filters pokemons and returns array of collected pokemons
    this.trainerSubscription$ = trainerService.trainer$.subscribe((trainer) => {
      this.pokemons = this.allPokemons.reduce((acc: any[], curr: any) => {
        if (trainer.pokemon.includes(curr.name)) {
          acc.push(curr);
        }
        return acc;
      }, []);
    });
  }

  /**
   * Slices collected pokemons based on page chosen
   * @param pageToSet
   */
  setPage(pageToSet: number): void {
    if (pageToSet > 1) {
      this.pokemonPage = this.pokemons.slice(
        this.pageLength * (pageToSet - 1),
        this.pageLength * pageToSet
      );
    } else {
      this.pokemonPage = this.pokemons.slice(0, this.pageLength);
    }
  }

  /**
   * Sets pokemonpage and page based on how many pokemons are collected.
   */
  ngOnInit(): void {
    this.pages = Array(Math.ceil(this.pokemons.length / this.pageLength)).map(
      (x, i) => i
    );
    this.pokemonPage = this.pokemons.slice(0, this.pageLength);
  }
}
