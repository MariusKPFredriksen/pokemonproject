import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { PokemonModule } from 'src/app/modules/pokemon-module';
import { PokemonService } from 'src/app/services/pokemon.service';

import { TrainerService } from 'src/app/services/trainer.service';
import { BASE_POKEMON_IMAGE_URL } from 'src/app/utils/api-utils';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css'],
})
export class CatalogueComponent {
  //Subscriptions
  public pokemonSubscription$: Subscription;
  //Local state
  public pokemons: PokemonModule[] = [];
  pokemonPage: any[] = [];
  pages: number[] = [];
  pageLength: number = 27;
  currentPage: number = 1;

  constructor(private readonly pokemonService: PokemonService) {
    //Subscribes to pokemon observable and slices array to create pages.
    this.pokemonSubscription$ = pokemonService.pokemons$.subscribe(
      (pokemons) => {
        this.pokemons = pokemons;
        this.pokemonPage =
          this.currentPage > 1
            ? (this.pokemonPage = this.pokemons.slice(
                this.pageLength * (this.currentPage - 1),
                this.pageLength * this.currentPage
              ))
            : (this.pokemonPage = this.pokemons.slice(0, this.pageLength));
        this.pages = Array(
          Math.ceil(this.pokemons.length / this.pageLength)
        ).map((x, i) => i);
      }
    );
  }

  /**
   * Slices pokemons based on what page is chosen
   * @param pageToSet
   */
  setPage(pageToSet: number): void {
    if (pageToSet > 1) {
      this.pokemonPage = this.pokemons.slice(
        this.pageLength * (pageToSet - 1),
        this.pageLength * pageToSet
      );
    } else {
      this.pokemonPage = this.pokemons.slice(0, this.pageLength);
    }
    this.currentPage = pageToSet;
  }
}
