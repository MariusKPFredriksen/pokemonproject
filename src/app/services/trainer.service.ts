import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TrainerModule } from '../modules/trainer-module';
import { BehaviorSubject, map, mergeMap, Observable, of } from 'rxjs';
import { BASE_TRAINER_URL, headers } from '../utils/api-utils';
import { AuthenticationService } from './authentication.service';
import { Router } from '@angular/router';
import { PokemonService } from './pokemon.service';

@Injectable({
  providedIn: 'root',
})
export class TrainerService {
  //Local state
  private _trainer$: BehaviorSubject<TrainerModule> =
    new BehaviorSubject<TrainerModule>({
      id: -1,
      username: '',
      pokemon: [],
    });

  constructor(
    private readonly http: HttpClient,
    private readonly router: Router,
    private readonly auth: AuthenticationService
  ) {
    // If there already is an id stored in localstorage, make api request
    // to get trainer/user data
    try {
      if (auth.isAuthenticated()) {
        const id: string | null = auth.GetTrainerId();
        id != null
          ? this.fetchTrainerById(id).subscribe({
              next: (res) => {
                this._trainer$.next(res);
                this.auth.AuthenticateTrainer(res.id);
              },
              error: (e) => {
                throw e;
              },
            })
          : null;
      }
    } catch (error) {
      console.error(error);
    }
  }

  /**
   * Getter function for trainer state
   */
  get trainer$(): Observable<TrainerModule> {
    return this._trainer$.asObservable();
  }

  /**
   * Reads id from localstorage.
   * @returns
   */
  public readTrainerID(): string {
    const resVal = localStorage.getItem('id');
    if (resVal === null) {
      return '-1';
    }
    return resVal;
  }

  /**
   * Fetches a trainer from server by username and updates _trainer state
   * with this trainer
   * @param uname username
   */
  private fetchTrainerByUsername(uname: string): Observable<TrainerModule[]> {
    return this.http.get<TrainerModule[]>(
      `${BASE_TRAINER_URL}?username=${uname}`
    );
  }

  /**
   * Fetches a trainer from server by id and updates _trainer state with this trainer
   * @param id
   */
  private fetchTrainerById(id: string): Observable<TrainerModule> {
    return this.http.get<TrainerModule>(`${BASE_TRAINER_URL}/${id}`);
  }

  /**
   * Creates a new trainer and updates trainer state with the
   * newly created user
   * @param uname username
   */
  private createTrainer(uname: string): Observable<TrainerModule[]> {
    return this.http.post<TrainerModule[]>(
      BASE_TRAINER_URL,
      {
        username: uname,
        pokemon: [],
      },
      {
        headers: headers,
      }
    );
  }

  /**
   * Tries to fetch a user/trainer from server, if user do not exist
   * creates a new user/trainer
   * @param uname username
   */
  public loginTrainer(uname: string): void {
    this.fetchTrainerByUsername(uname)
      .pipe(
        mergeMap((resp) => {
          if (resp.length < 1) {
            return this.createTrainer(uname);
          }
          return of(resp);
        })
      )
      .subscribe({
        next: (data) => {
          if (Array.isArray(data)) {
          }
          //Use concat because of the different
          //responses [{}] vs {}
          let result: TrainerModule[] = [];
          this._trainer$.next(result.concat(data)[0]);
          //Write id to localstorage
          this.auth.AuthenticateTrainer(result.concat(data)[0].id);
          //Redirect to catalogue
          this.router.navigateByUrl('/catalogue');
        },
        error: (e) => console.error(e),
      });
  }

  /**
   * Makes patch request to the trainer api to update catched pokemons.
   * @param currentPokemon
   * @param pokemon
   * @param trainerId
   */
  public addPokemon(
    currentPokemon: string[],
    pokemon: string,
    trainerId: number
  ): void {
    currentPokemon.push(pokemon);
    this.http
      .patch<TrainerModule>(
        `${BASE_TRAINER_URL}/${trainerId}`,
        {
          pokemon: currentPokemon,
        },
        {
          headers: headers,
        }
      )
      .subscribe({
        next: (trainer) => {
          this._trainer$.next(trainer);
        },
        error: (e) => console.error(e),
      });
  }

  /**
   * Make patch request to remove a pokemon from the api
   * @param currentPokemon
   * @param pokemon
   * @param trainerId
   */
  public removePokemon(
    currentPokemon: string[],
    pokemon: string,
    trainerId: number
  ): void {
    const index = currentPokemon.indexOf(pokemon);
    if (index > -1) {
      currentPokemon.splice(index, 1);
    }
    this.http
      .patch<TrainerModule>(
        `${BASE_TRAINER_URL}/${trainerId}`,
        {
          pokemon: currentPokemon,
        },
        {
          headers: headers,
        }
      )
      .subscribe({
        next: (trainer) => {
          this._trainer$.next(trainer);
        },
        error: (e) => console.error(e),
      });
  }

  /**
   * Clears local state
   */
  public clearState(): void {
    this._trainer$.next({
      id: -1,
      username: '',
      pokemon: [],
    });
  }
}
