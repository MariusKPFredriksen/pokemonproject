import { Injectable } from '@angular/core';
import { BASE_POKEMON_API_URL } from '../utils/api-utils';
import { HttpClient } from '@angular/common/http';
import { Observable, map, BehaviorSubject, Subscription } from 'rxjs';
import { BASE_POKEMON_IMAGE_URL } from '../utils/api-utils';
import { PokemonModule } from '../modules/pokemon-module';
import { TrainerService } from './trainer.service';

@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  //Subscription
  public trainerSubscription$: Subscription;
  public collected: string[] = [];

  private _pokemons$: BehaviorSubject<PokemonModule[]> = new BehaviorSubject<
    PokemonModule[]
  >([]);

  constructor(
    private readonly http: HttpClient,
    private readonly trainerService: TrainerService
  ) {
    //Subscribes to trainer service
    this.trainerSubscription$ = trainerService.trainer$.subscribe((trainer) => {
      this.collected = trainer.pokemon;
    });
    this.setLocalPokemons();
  }

  /**
   * Getter function for pokemons
   * @returns Observable<PokemonModule[]>
   */
  get pokemons$(): Observable<PokemonModule[]> {
    return this._pokemons$.asObservable();
  }

  /**
   * Makes request to pokemon api and fetches the 150 first
   */
  private fetchPokemons(): Observable<any> {
    return this.http.get<any>(`${BASE_POKEMON_API_URL}/pokemon?limit=151`);
  }

  /**
   * Writes pokemons to localstorage
   * @param pokemons
   */
  private writeToLocalStorage(pokemons: PokemonModule[]): void {
    localStorage.setItem('pokemons', JSON.stringify(pokemons));
  }

  /**
   * Reads pokemons from localstorage
   * @returns PokemonModule[]
   */
  private readFromLocalStorage(): PokemonModule[] | null {
    const retVal = localStorage.getItem('pokemons');
    if (retVal !== null) {
      return JSON.parse(retVal);
    }
    return null;
  }

  /**
   * If localstorage is empty for pokemons, make a request and update
   * state and write to localstorage, else read from localstorage and update state.
   */
  public setLocalPokemons() {
    if (this.readFromLocalStorage() === null) {
      this.fetchPokemons()
        .pipe(
          map((pokemons) => {
            return pokemons.results.map(
              (pokemon: PokemonModule, index: number) => {
                return {
                  name: pokemon.name,
                  imgUrl: `${BASE_POKEMON_IMAGE_URL}${index + 1}.png`,
                  id: index + 1,
                  collected: this.collected.includes(pokemon.name),
                };
              }
            );
          })
        )
        .subscribe({
          next: (pokemons: PokemonModule[]) => {
            this.writeToLocalStorage(pokemons);
            this._pokemons$.next(pokemons);
          },
          error: (e) => console.error(e),
        });
    } else {
      const updatePokemons = (): PokemonModule[] | undefined =>
        this.readFromLocalStorage()?.map((pokemon: PokemonModule) => {
          if (this.collected.includes(pokemon.name)) {
            return {
              ...pokemon,
              collected: true,
            };
          } else {
            return { ...pokemon, collected: false };
          }
        });

      this.writeToLocalStorage(updatePokemons() || []);
      this._pokemons$.next(updatePokemons() || []);
    }
  }
}
