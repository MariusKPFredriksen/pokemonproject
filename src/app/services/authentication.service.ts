import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  public _isSignedIn$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    false
  );

  /**
   * Getter functoin for local state
   */
  get isSignedIn$(): Observable<boolean> {
    this._isSignedIn$.next(this.isAuthenticated());
    return this._isSignedIn$.asObservable();
  }

  /**
   * Signs user out
   */
  public signOut(): void {
    this._isSignedIn$.next(false);
    localStorage.clear();
  }

  /**
   * Writes id to localstorage
   * @param id user/trainer id
   */
  public AuthenticateTrainer = (id: number): void => {
    localStorage.setItem('id', id.toString());
    this._isSignedIn$.next(true);
  };

  /**
   * Returns user/trainer id
   * @returns id or null
   */
  public GetTrainerId = (): string | null => {
    return localStorage.getItem('id');
  };

  /**
   * Checks if user is authenticated by reading id from localstorage
   * @returns boolean value
   */
  public isAuthenticated = (): boolean => {
    return localStorage.getItem('id') != null;
  };
}
