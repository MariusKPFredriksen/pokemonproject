import { inject } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivateFn,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

/**
 * Checks if user is authenticated if not redirects to login.
 * @param route
 * @param state
 * @returns
 */
export const AppAuthGuard: CanActivateFn = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot
) => {
  const auth = inject(AuthenticationService);
  const router = inject(Router);
  if (!auth.isAuthenticated()) {
    return router.navigateByUrl('/login');
  }
  return auth.isAuthenticated();
};
