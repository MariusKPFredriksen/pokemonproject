import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppAuthGuard } from './gurads/app-auth.guard';
import { CatalogueComponent } from './pages/catalogue/catalogue.component';
import { LandingComponent } from './pages/landing/landing.component';
import { TrainerComponent } from './pages/trainer/trainer.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/catalogue',
  },
  {
    path: 'login',
    component: LandingComponent,
  },
  {
    path: 'trainer',
    component: TrainerComponent,
    canActivate: [AppAuthGuard],
  },
  {
    path: 'catalogue',
    component: CatalogueComponent,
    canActivate: [AppAuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
