export interface TrainerModule {
  id: number;
  username: string;
  pokemon: string[];
}
