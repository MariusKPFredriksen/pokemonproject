export interface PokemonModule {
  id: number;
  name: string;
  imgUrl: string;
  collected: boolean;
}
