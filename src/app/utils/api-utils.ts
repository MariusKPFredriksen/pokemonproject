import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const headers = new HttpHeaders({
  'Content-Type': 'application/json',
  'x-api-key': environment.apiKey,
});

const BASE_TRAINER_URL =
  'https://noroff-api-production-83c7.up.railway.app/trainers';

const BASE_POKEMON_API_URL = 'https://pokeapi.co/api/v2';

const BASE_POKEMON_IMAGE_URL =
  'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/';

export {
  headers,
  BASE_TRAINER_URL,
  BASE_POKEMON_API_URL,
  BASE_POKEMON_IMAGE_URL,
};
