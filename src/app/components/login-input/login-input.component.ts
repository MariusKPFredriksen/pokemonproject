import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { TrainerService } from 'src/app/services/trainer.service';
import { faKeyboard } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-login-input',
  templateUrl: './login-input.component.html',
  styleUrls: ['./login-input.component.css'],
})
export class LoginInputComponent {
  inputStr: string = '';

  //Icon
  faKeyboard = faKeyboard;

  constructor(private readonly trainerService: TrainerService) {}

  /**
   * Handles submission of form.
   * @param form
   */
  public onSubmit(form: NgForm): void {
    this.trainerService.loginTrainer(form.value.loginForm);
  }
}
