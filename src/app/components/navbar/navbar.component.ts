import { Component } from '@angular/core';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Subscription } from 'rxjs';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent {
  //Subscriptioin
  public authSubscription$: Subscription;
  //Local state
  public signedIn: boolean = true;

  constructor(
    private router: Router,
    private readonly auth: AuthenticationService,
    private readonly trainer: TrainerService
  ) {
    //Subscribes to auth
    this.authSubscription$ = auth._isSignedIn$.subscribe((isSignedIn) => {
      this.signedIn = isSignedIn;
    });
  }

  //Icon
  faSignOutAlt = faSignOutAlt;

  /**
   * Signs user out
   */
  public signOut() {
    this.auth.signOut();
    this.trainer.clearState();
    this.router.navigate(['/', 'login']);
  }
}
