import { Component, Input, OnInit } from '@angular/core';
import { Subscription, tap } from 'rxjs';
import { PokemonModule } from 'src/app/modules/pokemon-module';
import { PokemonService } from 'src/app/services/pokemon.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.css'],
})
export class PokemonCardComponent {
  //Subscripton
  public trainerSubscription$: Subscription;

  //Local state
  public collectedPokemons: string[] = [];

  constructor(
    private readonly trainerService: TrainerService,
    private readonly pokemonService: PokemonService
  ) {
    //Subscribe to trainer service
    this.trainerSubscription$ = trainerService.trainer$.subscribe((trainer) => {
      this.collectedPokemons = trainer.pokemon;
    });
    this.pokemonService.setLocalPokemons();
  }

  // //Props
  @Input() page: string = '';
  @Input() pokemon: PokemonModule = {
    id: -1,
    name: '',
    imgUrl: '',
    collected: false,
  };

  /**
   * Capitalizes first letter of name
   * @returns
   */
  public getCapitalizedName(): string {
    return (
      this.pokemon.name.charAt(0).toUpperCase() + this.pokemon.name.slice(1)
    );
  }

  /**
   * Adds a pokemon to the catched pokemons
   * @param name name of the pokemon
   */
  public catchPokemon() {
    this.trainerService.addPokemon(
      this.collectedPokemons,
      this.pokemon.name,
      parseInt(this.trainerService.readTrainerID())
    );
    this.pokemon.collected = true;
    this.pokemonService.setLocalPokemons();
  }

  /**
   * Removes a collected pokemon
   * @param name
   */
  public removePokemon() {
    this.trainerService.removePokemon(
      this.collectedPokemons,
      this.pokemon.name,
      parseInt(this.trainerService.readTrainerID())
    );
    this.pokemon.collected = false;
    this.pokemonService.setLocalPokemons();
  }

  /**
   * Checks if pokemon has been collected
   * @returns
   */
  public isCollected(): boolean {
    return this.collectedPokemons.includes(this.pokemon.name);
  }
}
