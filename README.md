# PoKéMoN collector

A website for collecting and storing PoKéMoNs.

## Table of Contents

- [Description](#description)
  - [Login](#login)
  - [Catalogue](#catalogue)
  - [Trainer](#trainer)
- [Component tree](#component-tree)
- [Install](#install)
- [API](#API)
- [Contributors](#contributors)
- [License](#license)

## Description

PoKéMoN collector is an application for collecting PoKéMoN, developed for the third assignment in the front-end part of the Noroff fullstack .NET-course.

The application is a web application that communicates with an API in order to store PoKéMoN for a given trainer. The application also communicates with a [PokéAPI](https://pokeapi.co/) in order to fetch images and information about the different PoKéMoNs. The application is divided into three pages: Login, Catalogue and Trainer.

#### Login

A page for "logging" in a trainer. The login has no authentication, and simply creates a new trainer in the database through the API if the trainer doesn't exist. If the trainer exists, it fetches the PoKéMoN the trainer has collected and redirects to the catalogue component.

#### Catalogue

A catalogue displaying the first 151 PoKéMoNs from the first generation of PoKéMoNs. The Catalogue fetches data from the PokéAPI and displays it in a grid style, with pagination. Each PoKéMoN displayed is a pokemon-card component, and can be collected by the trainer by pressing the "Catch!" button under a PoKéMoN. This PoKéMoN will then be added to the trainer's collection and can be viewed in the Trainer-page.

#### Trainer

The trainer page lets the trainer view their collected PoKéMoNs. The trainer can also remove PoKéMoNs from their collection if they wish to, which will be reflected in the catalogue afterwards.

## Component tree

A component tree was designed before starting to develop the project. Although it does not 100% reflect the end-product, the component tree is still a good representation for the hierarchy of the components in the application.

The component tree can be found [here](./Component_tree.pdf).

## Install

This project uses [Angular](https://angular.io/) and [npm](https://npmjs.com). Go check them out if you don't have them locally installed.

In order to run the application locally, clone the project with `git`. Then install dependencies and run the application with `ng`:

```sh
git clone https://gitlab.com/MariusKPFredriksen/pokemonproject
npm install
ng serve
```

This will host the application locally on your machine, with the default port being 4200. The website can then be accessed on [http://localhost:4200](http://localhost:4200)

## Usage

The project is hosted through Vercel at [https://pd-pokemonproject.vercel.app/](https://pd-pokemonproject.vercel.app/), and can be tested fully through this link.

## API

The API used to store information about the trainer is hosted with [Railway](https://railway.app/) and can be accessed at [https://noroff-api-production-83c7.up.railway.app](https://noroff-api-production-83c7.up.railway.app).

In order to perform actions such as deleting, inserting or updating records, an API key is required. Fetching data can be done without an API key. The application hosted on Vercel has the API key stored locally as an environment variable, so the application can be fully tested there.

The API used to get information about the PoKéMoNs is a public API hosted at [https://pokeapi.co/](https://pokeapi.co/).

## Contributors

The application is developed by [Petter Dybdal](https://gitlab.com/petterdybdal) and [Marius Fredriksen](https://gitlab.com/MariusKPFredriksen).

## License

[MIT](LICENSE)
